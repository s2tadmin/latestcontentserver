package com.content.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * H2Db
 * 
 * @author Muthukrishnan
 * 
 */
public class H2Db {

	private static Logger log = Logger.getLogger(H2Db.class.getName());
	public static String DB = "contents";
	private static BasicDataSource config;
	
	public static final String CFILES="cfiles";
	public static final String EDOCUMENTS="edocuments";

	/**
	 * use a single connection pool
	 * 
	 * @return
	 */
	public static synchronized BasicDataSource getConnectionPool() {
		if (config == null) {
			config = new BasicDataSource();
			config.setDriverClassName("com.mysql.jdbc.Driver");
			config.setUrl("jdbc:mysql://192.168.1.99:3306/" + DB); 
			// jdbc url specific to your database,
			// eg. jdbc:mysql://127.0.0.1/yourdb
			config.setUsername("bluepc");
			config.setPassword("homeandlook");
		}
		return config;
	}

	/**
	 * Make sure this is called only once. This is used to setup our database
	 * for the first time
	 */
	public static void initDB() {
		Connection con = null;
		try {
			BasicDataSource connection = getConnectionPool();
			con = connection.getConnection();
			Statement stat = con.createStatement();
			// stat.execute("CREATE TABLE IF NOT EXISTS cfiles ( id bigint auto_increment primary key,	filename VARCHAR(500),location VARCHAR(2000) ,	contenttype VARCHAR(200) NULL,	fileurl VARCHAR(600))");
			StringBuilder sb = new StringBuilder();
			sb.append("CREATE TABLE IF NOT EXISTS `cfiles` (")
						.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
						.append("`filename` VARCHAR(2000) NOT NULL,")
						.append("`location` VARCHAR(2000) NOT NULL,")
						.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
						.append("`fileurl` VARCHAR(5000) NOT NULL,")
						.append("`companyid` INT(8) NOT NULL,")
						.append("`adate` DATETIME NULL DEFAULT NULL,")
						.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
						.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			

//			sb.setLength(0);
//			sb.append("CREATE TABLE IF NOT EXISTS `cfilehtml` (").append("`id` INT(20) NOT NULL AUTO_INCREMENT,").append("`cfile_id` INT(20) NOT NULL,")
//					.append("`html` LONGTEXT NOT NULL,").append("`adate` DATETIME NOT NULL,")
//					.append("`udate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,").append("PRIMARY KEY (`id`),")
//					.append("INDEX `fk_cfile_id` (`cfile_id`),")
//					.append("CONSTRAINT `fk_cfile_id` FOREIGN KEY (`cfile_id`) REFERENCES `cfiles` (`id`) ON DELETE CASCADE )");
//			stat.execute(sb.toString());
			
			sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `edocuments` (")
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			
			sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `ofiles` (")
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			
			sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `kfiles` (")
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			
			sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `rfiles` (") // resignation related files
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
          
           sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `cmpdocuments` (")
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			
		sb.setLength(0);
			sb.append("CREATE TABLE IF NOT EXISTS `lms` (")
					.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
					.append("`filename` VARCHAR(2000) NOT NULL,")
					.append("`location` VARCHAR(2000) NOT NULL,")
					.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
					.append("`fileurl` VARCHAR(5000) NOT NULL,")
					.append("`companyid` INT(8) NOT NULL,")
					.append("`adate` DATETIME NULL DEFAULT NULL,")
					.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
					.append("PRIMARY KEY (`id`) )");
			stat.execute(sb.toString());
			
		sb.setLength(0);
		sb.append("CREATE TABLE IF NOT EXISTS `intranet` (")
				.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
				.append("`filename` VARCHAR(2000) NOT NULL,")
				.append("`location` VARCHAR(2000) NOT NULL,")
				.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
				.append("`fileurl` VARCHAR(5000) NOT NULL,")
				.append("`companyid` INT(8) NOT NULL,")
				.append("`adate` DATETIME NULL DEFAULT NULL,")
				.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
				.append("PRIMARY KEY (`id`) )");
		stat.execute(sb.toString());
		
		sb.setLength(0);
		sb.append("CREATE TABLE IF NOT EXISTS `configfile` (")
				.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
				.append("`filename` VARCHAR(2000) NOT NULL,")
				.append("`location` VARCHAR(2000) NOT NULL,")
				.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
				.append("`fileurl` VARCHAR(5000) NOT NULL,")
				.append("`companyid` INT(8) NOT NULL,")
				.append("`adate` DATETIME NULL DEFAULT NULL,")
				.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
				.append("PRIMARY KEY (`id`) )");
		stat.execute(sb.toString());
		
		sb.setLength(0);
		sb.append("CREATE TABLE IF NOT EXISTS `bgvfile` (")
				.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
				.append("`filename` VARCHAR(2000) NOT NULL,")
				.append("`location` VARCHAR(2000) NOT NULL,")
				.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
				.append("`fileurl` VARCHAR(5000) NOT NULL,")
				.append("`companyid` INT(8) NOT NULL,")
				.append("`adate` DATETIME NULL DEFAULT NULL,")
				.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
				.append("PRIMARY KEY (`id`) )");
		stat.execute(sb.toString());
		
		sb.setLength(0);
		sb.append("CREATE TABLE IF NOT EXISTS `bgvapproverfile` (")
				.append("`id` INT(11) NOT NULL AUTO_INCREMENT,")
				.append("`filename` VARCHAR(2000) NOT NULL,")
				.append("`location` VARCHAR(2000) NOT NULL,")
				.append("`contenttype` VARCHAR(1000) NOT NULL DEFAULT '',")
				.append("`fileurl` VARCHAR(5000) NOT NULL,")
				.append("`companyid` INT(8) NOT NULL,")
				.append("`adate` DATETIME NULL DEFAULT NULL,")
				.append("`udate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,")
				.append("PRIMARY KEY (`id`) )");
		stat.execute(sb.toString());
		
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e);
		} finally {
			close(con);
		}
	}
	

	/**
	 * Save file and return generated id
	 * 
	 * @param filename
	 * @param contenttype
	 * @param size
	 * @param fileurl
	 * @param location
	 * @return
	 */
	public static int saveFileInformation(String filename, String contenttype, String fileurl, String location, String companyid, String tableName) {
		Connection con = null;
		ResultSet rs = null;
		int autoId = 0;
		BasicDataSource connection = getConnectionPool();
		try {
			con = connection.getConnection();
			// prepared statement
			PreparedStatement prep = con.prepareStatement("INSERT INTO "+tableName+" (filename, contenttype, fileurl, location,companyid,adate) VALUES (?,?,?,?,?,NOW())",
					Statement.RETURN_GENERATED_KEYS);
			// Values
			prep.setString(1, filename);
			prep.setString(2, contenttype);
			prep.setString(3, fileurl);
			prep.setString(4, location);
			prep.setString(5, companyid);
			prep.execute();
			rs = prep.getGeneratedKeys();
			rs.next();
			autoId = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e);
		} finally {
			close(rs);
			close(con);
		}
		return autoId;
	}

	/**
	 * Get file information
	 * 
	 * @param connectionName
	 * @return
	 */
	public static JSONObject getFileInformation(int fileid, String tableName) {
		Connection con = null;
		ResultSet rs = null;
		tableName = StringEscapeUtils.escapeSql(tableName);
		JSONObject obj = new JSONObject();
		BasicDataSource connection = getConnectionPool();
		try {
			con = connection.getConnection();
			Statement stat = con.createStatement();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT id,filename, contenttype, fileurl, location FROM ").append(tableName).append(" WHERE id=").append(fileid).append(" LIMIT 1");
			rs = stat.executeQuery(sb.toString());
			while (rs.next()) {
				obj.put("id", rs.getInt(1));
				obj.put("filename", rs.getString(2));
				obj.put("contenttype", rs.getString(3));
				obj.put("fileurl", rs.getString(4));
				obj.put("location", rs.getString(5));
			}
		} catch (SQLException e) {
			log.error(e);
		} catch (JSONException e) {
			log.error(e);
		} finally {
			close(rs);
			close(con);
		}
		return obj;
	}

	/**
	 * Delete File.
	 * 
	 * @author Kurra Raghu
	 * @param fileid
	 * @return statuscode (1 or 0)
	 */
	public static int deleteFileInformation(int fileid, String tableName) {
		int statuscode = 0;
		Connection con = null;
		tableName = StringEscapeUtils.escapeSql(tableName);
		BasicDataSource connection = getConnectionPool();
		try {
			con = connection.getConnection();
			Statement stat = con.createStatement();
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ").append(tableName).append(" WHERE id=").append(fileid).append(" LIMIT 1");
			statuscode = stat.executeUpdate(sb.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			log.error(e);
		} finally {
			close(con);
		}
		return statuscode;
	}
 

//	public static int saveFileHtml(int fileId, String html) {
//		Connection con = null;
//		ResultSet rs = null;
//		int autoId = 0;
//		try {
//			BasicDataSource connection = getConnectionPool();
//			con = connection.getConnection();
//			// prepared statement
//			PreparedStatement prep = con.prepareStatement("INSERT INTO cfilehtml (file_id, html,adate) VALUES (?,?,NOW())", Statement.RETURN_GENERATED_KEYS);
//			prep.setInt(1, fileId);
//			prep.setString(2, html);
//			prep.execute();
//			rs = prep.getGeneratedKeys();
//			rs.next();
//			autoId = rs.getInt(1);
//		} catch (SQLException e) {
//			log.error(e);
//		} finally {
//			close(rs);
//			close(con);
//		}
//		return autoId;
//	}

	public static void close(AutoCloseable con) {
		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
				log.error(e);
			}
		} else {
			log.info("Connection  closed already");
		}
	}

}
