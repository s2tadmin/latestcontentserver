package com.content.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.content.db.H2Db;
import com.content.utility.DocumentWarehouseUtility;
import com.content.utility.PBEEncrypter;
import com.content.utility.Slugify;

/**
 * Servlet implementation class DocumentWarehouse
 */
@WebServlet(value = "/documentwarehouse", loadOnStartup = 1)
public class DocumentWarehouse extends HttpServlet {
	private static Logger log = Logger.getLogger(DocumentWarehouse.class);

	private static final long serialVersionUID = 1L;

	private DiskFileItemFactory factory = new DiskFileItemFactory();
	private static PBEEncrypter pbe = new PBEEncrypter();
	private String WAREHOUSENAME = "warehouse";
	//private final String IP_RANGE = "144.76.83.234";

	/**
	 * TOKEN contains parameter name of encrypted fileid value
	 */
	private String PARM_TOKEN = "token";
	private String PARM_COMPANYID = "companyid";
	private String PARM_DOCUMENTLOCATION = "documentlocation";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DocumentWarehouse() {
		super();
	}

	@Override
	public void init() throws ServletException {
		// Create a factory for disk-based file items
		// Configure a repository (to ensure a secure temp location is used)
		ServletContext servletContext = this.getServletConfig().getServletContext();
		File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		factory.setRepository(repository);
		H2Db.initDB();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String documentLocation = getStringParameter(request.getParameter(PARM_DOCUMENTLOCATION), null);
		String tableName = DocumentWarehouseUtility.getTableName(documentLocation);
		if (tableName == null) {
			response.getWriter().write("Invalid Location:" + documentLocation);
			log.error("doGet Invalid file location  tableName :" + tableName + ",documentLocation:" + documentLocation);
			response.setStatus(401);
			return;
		}
		int fileId = getIntegerParameter(pbe.decrypt(request.getParameter(PARM_TOKEN)));
		if (fileId == 0) {
			response.getWriter().write("Invalid token");
			response.setStatus(401);
			return;
		}
		try {
			DocumentWarehouseUtility.sendFile(response, fileId, tableName);
		} catch (JSONException e) {
			log.error(e);
		}

		// String view = getStringParameter(request.getParameter("view"), "");
		// if (view.equals("html")) {
		// try {
		// DocumentWarehouseUtility.sendHtml(response, fileId);
		// } catch (JSONException e) {
		// log.error(e);
		// }
		// } else {
		// try {
		// DocumentWarehouseUtility.sendFile(response, fileId);
		// } catch (JSONException e) {
		// log.error(e);
		// }
		// }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		// read the path related parameters
		String companyid = getStringParameter(request.getParameter(PARM_COMPANYID), null);
		if (companyid == null) {// client location
			response.getWriter().write("failed to recognize company");
			log.error("doPost failed to recognize company- companyid:" + companyid);
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		String documentLocation = getStringParameter(request.getParameter(PARM_DOCUMENTLOCATION), null);
		if (documentLocation == null) {// document location
			response.getWriter().write("failed to recognize documentlocation : " + documentLocation);
			log.error("doPost failed to recognize documentlocation :" + documentLocation);
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		String tableName = DocumentWarehouseUtility.getTableName(documentLocation);
		if (tableName == null) {
			// Unauthorized
			response.getWriter().write("Invalid file location " + documentLocation);
			log.error("doPost Invalid file location  tableName :" + tableName + ",documentLocation:" + documentLocation);
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		if (!ServletFileUpload.isMultipartContent(request)) {
			response.getWriter().write("must be multipart/form-data");
			response.setHeader("refresh", "1;url=" + request.getContextPath());
			response.setStatus(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
			log.error("doPost uploaded data is not multipartContent");
			return;
		}

		// set the base directory
		documentLocation = Slugify.slugify(documentLocation);
		String fileurl = "http:///contentserver/" + WAREHOUSENAME + "/" + companyid + "/" + documentLocation;
		String warehouseDirPath = getServletContext().getRealPath("/" + WAREHOUSENAME) + File.separator + companyid + File.separator + documentLocation;

		File dir = new File(warehouseDirPath);
		// make the directories required for storage
		if (!dir.isDirectory() && !dir.mkdirs()) {
			log.error("doPost Faield to create directory,Path: " + warehouseDirPath);
		}

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setFileSizeMax(100 * 1024 * 1024);

		UploadProgressListener uploadProgressListener = new UploadProgressListener();
		upload.setProgressListener(uploadProgressListener);
		request.getSession().setAttribute("uploadProgressListener", uploadProgressListener);

		try {
			// Parse the request
			List<FileItem> list = upload.parseRequest(request);
			JSONArray array = DocumentWarehouseUtility.addNewFiles(list, fileurl, warehouseDirPath, companyid, tableName, pbe);
			if (array.length() > 0) {
				// file uploaded successfully
				response.setStatus(HttpServletResponse.SC_OK);
			} else {
				log.error("Faield to upload file " + list.toString());
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
			JSONObject respobj = new JSONObject();
			respobj.put("files", array);
			response.getWriter().write(respobj.toString());
		} catch (FileUploadException | JSONException e) {
			log.error(e);
			response.getWriter().write("Upload Failed");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String cIpRange = DocumentWarehouseUtility.getClientIpRange(request.getRemoteAddr());
//		if (!IP_RANGE.equals(cIpRange)) {
//			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//		}
//		cIpRange = null;
		String documentLocation = getStringParameter(request.getParameter(PARM_DOCUMENTLOCATION), null);
		String tableName = DocumentWarehouseUtility.getTableName(documentLocation);
		if (tableName == null) {
			response.getWriter().write("Invalid file location:" + documentLocation);
			log.error("doDelete Invalid file location  tableName :" + tableName + ",documentLocation:" + documentLocation);
			response.setStatus(401);
			return;
		}
		int fileid = getIntegerParameter(pbe.decrypt(request.getParameter(PARM_TOKEN)));
		if (fileid == 0) {
			response.getWriter().write("Invalid token");
			log.error("doDelete Invalid token :" + fileid);
			response.setStatus(401);
			return;
		}
		int status = H2Db.deleteFileInformation(fileid, tableName);
		if (status > 0) {
			// OK
			response.setStatus(200);
		} else {
			// No Content
			response.setStatus(204);
		}
		response.getWriter().write(status);
	}

	public static String getStringParameter(Object requestValue, String defaultVal) {
		return requestValue != null ? requestValue.toString() : defaultVal;
	}

	public static int getIntegerParameter(Object requestValue) {
		try {
			return requestValue != null ? Integer.valueOf(requestValue.toString()) : 0;
		} catch (Exception e) {
			log.error(e);
		}
		return 0;
	}

}
