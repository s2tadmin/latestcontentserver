package com.content.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * This is for encrypting file id. Don't  Modify this class if it is modified than encryption keys should be changed.
 * Solution Project PBEEncrypter class and this class should have same salt,ITERATECOUNT,passPHARSE and algorithm.
 * @author Kurra Raghu
 *
 */
public class PBEEncrypter {
	private static Logger log = Logger.getLogger(PBEEncrypter.class.getName());

	private String passPHARSE;
	// Iteration count
	private static final int ITERATECOUNT = 15;
	private static Cipher dcipher, ecipher;
	// 8-bytes Salt
	private static final byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32, (byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03 };

	/**
	 * use default encryption passPHARSE key
	 */
	public PBEEncrypter() {
		this.passPHARSE = "privatekey1010518379";
		this.init();
	}

	/**
	 * use passed encryption passPHARSE key
	 * 
	 * @param passPharse
	 */
	public PBEEncrypter(String passPharse) {
		this.passPHARSE = passPharse;
		this.init();
	}

	/**
	 * Responsible for setting, initializing this object's encrypter and
	 * decrypter Chipher instances
	 */
	private void init() {
		try {
			// Generate a temporary key. In practice, you would save this key
			// Encrypting with DES Using a Pass Phrase
			KeySpec keySpec = new PBEKeySpec(passPHARSE.toCharArray(), salt, ITERATECOUNT);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
			ecipher = Cipher.getInstance(key.getAlgorithm());
			dcipher = Cipher.getInstance(key.getAlgorithm());

			// Prepare the parameters to the cipthers
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, ITERATECOUNT);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			log.error(e);
		} catch (InvalidKeySpecException e) {
			log.error(e);
		} catch (NoSuchPaddingException e) {
			log.error(e);
		} catch (NoSuchAlgorithmException e) {
			log.error(e);
		} catch (InvalidKeyException e) {
			log.error(e);
		}
	}

	/**
	 * Encrpt Password with URL safe using Apache base64
	 * @param value
	 * @return
	 */
	public final synchronized String encrypt(String value) {
		if (value != null) {
			try {
				// Encode the string into bytes using utf-8
				byte[] utf8 = value.getBytes("UTF8");
				// Encrypt ,Encode bytes to base64 to get a string
				new Base64();
				return Base64.encodeBase64URLSafeString(ecipher.doFinal(utf8));
			} catch (BadPaddingException e) {
				log.error(e);
			} catch (IllegalBlockSizeException e) {
				log.error(e);
			} catch (UnsupportedEncodingException e) {
				log.error(e);
			}
		}
		return "";
	}

	/**
	 * Decrpt password
	 * To decrypt the encryted password url safe
	 * @param encValue
	 * @return
	 */
	public final synchronized String decrypt(String encValue) {
		try {
			byte[] utf8 = dcipher.doFinal(new Base64().decode(encValue));
			return new String(utf8, "UTF8");
		} catch (BadPaddingException e) {
			log.error(e);
		} catch (IllegalBlockSizeException e) {
			log.error(e);
		} catch (UnsupportedEncodingException e) {
			log.error(e);
		} catch (Exception e) {
			log.error(e);
		}
		return "";
	}

}