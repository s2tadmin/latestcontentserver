package com.content.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.content.db.H2Db;

public final class DocumentWarehouseUtility {
	private static Logger log = Logger.getLogger(DocumentWarehouseUtility.class);

	private DocumentWarehouseUtility() {

	}

	/* Document Location */
	public static final String CANDIDATERESUME = "resumes";// FILELOC_CFILES
	public static final String EMPLOYEE_DOCUMENTS = "employeedocuments";// FILELOC_EDOCUMENTS
	public static final String USERATACHMENTS = "useratachments";// FILELOC_EDOCUMENTS
	public static final String KNOWLEDGEBANK = "knowledgeBank";// FILELOC_KNOWLEDGEBANK
	public static final String TRAININGATTACHMENTS = "trainingattachments";// FILELOC_ORGANIZATION
	public static final String COMPANYPOLICY = "companypolicy";// FILELOC_ORGANIZATION
	public static final String RESIGNATION = "resignation"; //resignation attachment
	public static final String LEARNING = "learning"; //learning attachment
	public static final String COMPANY_DOCUMENTS = "companydocuments"; //COMPANY DOCUMENTS
	public static final String INTRANETFILES = "intranetfiles"; //INTRANET FILES
	public static final String ONBOARDCHECKLIST = "onboardchecklist";// FILELOC_CFILES
	public static final String CONFIGFILE = "configfile";//birthday image
	public static final String BGVFILE = "bgvfile";//bgv file
	public static final String BGVAPPROVERFILE = "bgvapproverfile";
	public static final String GOALATTACHMENTS = "goalattachments";

	/* Table Name-- (fileloc) */
	private static final String TABLE_CFILES = "cfiles";
	private static final String TABLE_EDOCUMENTS = "edocuments";
	private static final String TABLE_KNOWLEDGEBANK = "kfiles"; // Knowledge connect files
	private static final String TABLE_ORGANIZATION = "ofiles";
	private static final String TABLE_RESIGNATION = "rfiles"; //resignation related files
	private static final String TABLE_CMPDOCUMENTS = "cmpdocuments";
	private static final String TABLE_LEARNING = "lms";
	private static final String TABLE_INTRANET = "intranet";
	public static final String  TABLE_CONFIGFILE = "configfile";//birthday related files
	public static final String TABLE_BGVFILE = "bgvfile";//bgv file
	public static final String TABLE_BGVAPPROVERFILE = "bgvapproverfile";//bgv file
	
	public static String getTableName(String documentLocation) {
		TreeMap<String, String> loc = new TreeMap<String, String>();
		loc.put(CANDIDATERESUME, TABLE_CFILES);
		loc.put(EMPLOYEE_DOCUMENTS, TABLE_EDOCUMENTS);
		loc.put(USERATACHMENTS, TABLE_EDOCUMENTS);
		loc.put(KNOWLEDGEBANK, TABLE_KNOWLEDGEBANK);
		loc.put(TRAININGATTACHMENTS, TABLE_ORGANIZATION);
		loc.put(GOALATTACHMENTS, TABLE_ORGANIZATION);
		loc.put(COMPANYPOLICY, TABLE_ORGANIZATION);
		loc.put(RESIGNATION, TABLE_RESIGNATION);
		loc.put(COMPANY_DOCUMENTS, TABLE_CMPDOCUMENTS);
		loc.put(LEARNING, TABLE_LEARNING);
		loc.put(INTRANETFILES, TABLE_INTRANET);
		loc.put(ONBOARDCHECKLIST, TABLE_CFILES);
		loc.put(CONFIGFILE, TABLE_CONFIGFILE);
		loc.put(BGVFILE, TABLE_BGVFILE);
		loc.put(BGVAPPROVERFILE, TABLE_BGVAPPROVERFILE);
		return loc.get(documentLocation);
	}

	public static JSONArray addNewFiles(List<FileItem> list, String fileurl, String warehouseDirPath, String companyid, String tableName, PBEEncrypter pbe)
			throws IOException {
		JSONArray array = new JSONArray();
		for (FileItem f : list) {
			if (!f.isFormField() && f.getName().length() > 0) {
				String uploadfilename = f.getName().substring(f.getName().lastIndexOf(File.separator) + 1);
				int pos = uploadfilename.lastIndexOf(".");
				String ext = "";
				if (pos > 0) {
					ext = uploadfilename.substring(pos, uploadfilename.length());
					uploadfilename = uploadfilename.substring(0, pos);
				}
				uploadfilename = Slugify.slugify(generateUniqueFileName(uploadfilename)) + ext;
				fileurl = fileurl + "/" + uploadfilename;
				String location = warehouseDirPath + File.separator + uploadfilename;
				File doc = new File(location);
				InputStream is = null;
				OutputStream os = null;
				try {
					is = f.getInputStream();
					os = new FileOutputStream(doc);
					IOUtils.copy(is, os);
				} finally {
					if (null != is) {
						is.close();
					}
					if (null != os) {
						os.close();
					}
				}
				int fileid = H2Db.saveFileInformation(f.getName(), f.getContentType(), fileurl, location, companyid, tableName);
				if (fileid > 0) {
					try {
						JSONObject obj = new JSONObject();
						obj.put("token", pbe.encrypt(fileid + ""));
						obj.put("filename", f.getName());
						obj.put("contenttype", f.getContentType());
						obj.put("sizeinbytes", f.getSize());
						obj.put("fileurl", fileurl);
						array.put(obj);
					} catch (JSONException e) {
						log.error(e);
					}
				} else {
					if (doc.exists() && !doc.delete()) {
						log.error("doPost File faield to delete, file:" + doc.getAbsolutePath());
					}
				}

			}
		}
		return array;
	}

	/**
	 * @param response
	 * @param fileId
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void sendFile(HttpServletResponse response, int fileId, String tableName) throws IOException, JSONException {
		JSONObject fileInformation = H2Db.getFileInformation(fileId, tableName);
		// Find this file id in database to get file name, and file type
		// You must tell the browser the file type you are going to send
		// for example application/pdf, text/plain, text/html, image/jpg
		response.setContentType(fileInformation.getString("contenttype"));
		// Make sure to show the download dialog
		response.setHeader("Content-disposition", "attachment; filename=" + fileInformation.getString("filename"));

		File file = new File(fileInformation.getString("location"));

		// This should send the file to browser
		OutputStream out = response.getOutputStream();
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
		out.flush();
	}

	public static String getClientIpRange(String remoteAddr) {
		StringTokenizer toke = new StringTokenizer(remoteAddr, ".");
		int dots = 0;
		String byte1 = "";
		String byte2 = "";
		while (toke.hasMoreTokens()) {
			++dots;
			// if we've reached the second dot, break and check out the index
			// value
			if (dots == 1) {
				byte1 = toke.nextToken();
			} else {
				byte2 = toke.nextToken();
				break;
			}
		}
		// Piece together half of the client IP address so it can be compared
		// with
		// the forbidden range represented by IPFilter.IP_RANGE
		return byte1 + "." + byte2;
	}

	/**
	 * @param fileName
	 *            ( With out extinction)
	 * @return Random File Name or String
	 */
	private static String generateUniqueFileName(String fileName) {
		SimpleDateFormat format = new SimpleDateFormat("MMddyy");
		String datetime = format.format(new Date());
		String rndchars = RandomStringUtils.randomAlphanumeric(8);
		return rndchars + "_" + datetime + "_" + fileName;
	}

	/**
	 * @author Kurra Raghu
	 * @param response
	 * @param fileId
	 * @throws IOException
	 * @throws JSONException
	 */
	// public static void sendHtml(HttpServletResponse response, int fileId,
	// String tableName) throws IOException, JSONException {
	// response.setHeader("Content-Type", "text/html");
	// PrintWriter writer = null;
	// FileInputStream inputStream = null;
	// try {
	// String html = H2Db.getFileHtml(fileId, tableName);
	// if (html != null) {
	// html = html.replace("</style>", ".page {border: 0px;}</style>");
	// writer = response.getWriter();
	// writer.write(html);
	// } else {
	// // Send File data if IT is Not HTML View
	// JSONObject fileInformation = H2Db.getFileInformation(fileId, tableName);
	// inputStream = new FileInputStream(new
	// File(fileInformation.getString("location")));
	// writer = response.getWriter();
	// int c;
	// while ((c = inputStream.read()) != -1) {
	// writer.write(c);
	// }
	// }
	// } finally {
	// if (null != writer) {
	// writer.close();
	// }
	// if (inputStream != null) {
	// inputStream.close();
	// writer.close();
	// }
	//
	// }
	//
	// }

}
